import ConfigStore from "../app/stores/configStore.js";
import assert from "assert";

describe("ConfigStore", () => {
  describe("getConfig", () => {
    it("should be able to query succesive keys using '.' separator", () => {
      let c = new ConfigStore({
        "test": {
          "a": { "value": 1 },
          "b": { "value": 2 },
          "c": { "value": 3 }
        },
      });
      assert.equal(c.getConfig("test.b.value"), 2);
    });

    it("should return null, if the value cannot be found", () => {
      let c = new ConfigStore({
        "test": {"a": 1, "b": 2}
      });
      assert.equal(c.getConfig("foo"), null);
      assert.equal(c.getConfig("test.c"), null);
      assert.equal(c.getConfig("test.a.foobar"), null);
    });
  });
});
