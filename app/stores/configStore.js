export default class ConfigStore {
  constructor(config) {
    this._config = config;
  }

  getConfig(key) {
    return key.split('.').reduce(
      (acc, el) => (typeof acc === "object" && acc[el]) ? acc[el] : null,
      this._config
    );
  }
}
