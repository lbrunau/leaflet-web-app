
import * as Rx from 'rxjs';

const MapStore = (() => {
  return {
    zoom: new Rx.Subject(0),
    center: new Rx.Subject([0,0]),
    selectedFeature: new Rx.BehaviorSubject(null)
  };
})();

export default MapStore;
