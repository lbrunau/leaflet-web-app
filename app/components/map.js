import L from "leaflet";
import './zoomDisplay.js';
import 'leaflet.fullscreen';
import DataLoader from "../util/dataLoader";
import MapStore from "../stores/mapStore";

import "../css/map.css";
import "leaflet/dist/leaflet.css";
import "leaflet.fullscreen/Control.FullScreen.css";

export default class Map {
  constructor(domElement) {
    const that = this;

    this.selected = null;
    this.highlighted = [];

    let tileLayers = this._createTileLayers(
      window.configStore.getConfig("map.tileLayers")
    );

    var options = {
      layers: Object.values(tileLayers).reverse()
    };

    this.map = L.map(
      domElement,
      Object.assign(options, window.configStore.getConfig("map.leafletOptions"))
    );

    this.map.on('click', () => {
      //that._resetSelect()
    });
    this.map.on('moveend', () => {
      MapStore.center.next(that.map.getCenter())
    });
    this.map.on('zoomend', () => {
      MapStore.zoom.next(that.map.getZoom())
    });

    if(window.configStore.getConfig('map.controls.scale')) {
      L.control.scale().addTo(this.map);
    }
    if(window.configStore.getConfig('map.controls.fullscreen')) {
      L.control.fullscreen().addTo(this.map);
    }
    if(window.configStore.getConfig('map.controls.zoomLevel')) {
      L.control.zoomDisplay().addTo(this.map);
    }

    const finalizeMapCreation = () => {
      MapStore.zoom.next(that.map.getZoom());
      MapStore.center.next(that.map.getCenter());
    };

    const vectorLayerConfig = window.configStore.getConfig("map.geoJSONLayers");
    this.styles = this._createLayerStyles(
      window.configStore.getConfig("map.styles"),
      vectorLayerConfig);
    this._createVectorLayers(vectorLayerConfig).then((vectorLayers) => {
      that._createLayerControl(tileLayers, vectorLayers);
    }).then(finalizeMapCreation);
  }

  _createLayerControl(tileLayers, vectorLayers) {
    if(Object.keys(tileLayers).length > 1 || Object.keys(vectorLayers).length > 1) {
      L.control.layers(tileLayers, vectorLayers).addTo(this.map);
      // activate the first entry by default
      window.setTimeout(() => {
        const layerLinks =
          document.getElementsByClassName('leaflet-control-layers-selector');
        if(layerLinks.length > 0) {
          layerLinks[0].click();
        }
      }, 0);
    }
  }

  _createLayerStyles(defaultStyles, layerConfig) {
    const styles = {};
    layerConfig.forEach((l) => {
      styles[l.name] = {};

      Object.keys(defaultStyles).forEach((st) => {
        styles[l.name][st] = (l.styles && l.styles[st]) ? Object.assign({}, defaultStyles[st], l.styles[st]) : defaultStyles[st];
      });
    });
    return styles;
  }

  _highlight(e) {
    // const layer = e.target;
    // layer.setStyle(this.styles.highlighted);
    // this.highlighted.push(layer);
  }

  _resetHighlight() {
    // this.highlighted.map(l => l.setStyle(this.styles.default));
    // this.hightlighted = [];
    //
    // MapStore.selectedFeature = null;
  }

  _select(e, layerName) {
    e.originalEvent.stopPropagation();
    e.originalEvent.preventDefault();
    const layer = e.target;
    MapStore.selectedFeature.next(e.target.feature);

    this._resetSelect();

    layer.setStyle(this.styles[layerName].selected);
    this.selected = {
      layerName: layerName,
      feature: layer
    };

    return false;
  }

  _resetSelect() {
    if(this.selected) {
      this.selected.feature.setStyle(this.styles[this.selected.layerName].default);
    }
    this.selected = null;
  }

  _createTileLayers(tileLayerConfig) {
    let tileLayers = {};

    if(Array.isArray(tileLayerConfig)) {
      tileLayerConfig.forEach((l) => {
        tileLayers[l.name] = L.tileLayer(l.url, l);
      });
    }

    return tileLayers;
  }

  _createVectorLayers(vectorLayerConfig) {
    const that = this;
    let vectorLayerPromises = [];

    if(Array.isArray(vectorLayerConfig)) {
      vectorLayerConfig.forEach((layerConfig) => {
        vectorLayerPromises.push(
          DataLoader.getRequest(layerConfig.url).then((data) => {
            const l = L.geoJSON(JSON.parse(data), {
              pointToLayer: (point, latlon) => L.marker(latlon),
              style: () => this.styles[layerConfig.name].default,
              onEachFeature: (f, layer) => {
                layer.on({
                  mouseover: (e) => that._highlight(e),
                  mouseout: (e) => that._resetHighlight(e),
                  click: (e) => that._select(e, layerConfig.name)
                });
              }
            });
            if(layerConfig.active) {
              that.map.addLayer(l);
            }
            return l;
          })
        )
      });
    }

    return Promise.all(vectorLayerPromises).then((layers) => {
      let vectorLayers = {};
      layers.forEach((l, i) => {
        vectorLayers[vectorLayerConfig[i].name] = l;
      });
      return vectorLayers;
    });
  }
}
