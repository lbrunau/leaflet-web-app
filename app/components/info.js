import TemplateLoader from "../util/templateLoader";
import MapStore from "../stores/mapStore";

import "../css/map_info.css";

export default class Info {
  constructor(domEl, templateName) {
    this.el = domEl;
    this.template = templateName;
    TemplateLoader.load(templateName);

    MapStore.selectedFeature.subscribe((value) => {
      if(!value) {
        this.hide();
      } else if(value.properties) {
        this.show(value.properties);
      }
    });
  }

  show(data) {
    this.el.classList.add("active");
    this.el.innerHTML = TemplateLoader.render(this.template, data);
  }

  hide() {
    this.el.classList.remove("active");
  }
}
