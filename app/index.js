import "@babel/polyfill";
import { filter } from 'rxjs/operators';
import Info from "./components/info.js";
import Map from "./components/map.js";
import ConfigStore from "./stores/configStore.js";
import DataLoader from "./util/dataLoader.js";

import "./css/default.css";

(() => {
  // get basePath of project
  // webpack will put the bundle as a script tag into body. If all other
  // scripts are in the header, we can use the path of the first script in
  // the body to get the base path.
  const basePath = (() => {
    const bodyScriptTags = document.body.getElementsByTagName("script");
    if(bodyScriptTags.length > 0) {
      const bundleLocation = bodyScriptTags[0].getAttribute("src");
      return bundleLocation.substring(0, bundleLocation.lastIndexOf("/") + 1);
    }
    return "/"; // fallback
  })();

  const configUrl = basePath + "config.json";
  DataLoader.cleanCache(configUrl);
  DataLoader.getRequest(configUrl).then((configData) => {
    window.configStore = new ConfigStore(JSON.parse(configData));

    const info = new Info(document.getElementById("feature-info"), "featureInfo.tpl.html");

    new Map(document.getElementById("map"));
  });
})();
