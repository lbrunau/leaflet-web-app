const DataLoader = (() => {
  const contentTypes = {
    "json": "application/json,application/vnd.application+json,application/vnd.api+json"
  };

  return {
    cleanCache: function(url) {
      if(window.caches) {
        window.caches.delete(url);
      }
    },

    getRequest: function(url, type="json") {
      return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
          if(xhr.readyState == XMLHttpRequest.DONE) {
            if(xhr.status == 200) {
              resolve(xhr.responseText);
            } else {
              reject(xhr.statusText, xhr.status);
            }
          }
        };
        xhr.open("GET", url, true);
        if(contentTypes[type]) {
          xhr.setRequestHeader("Accept", contentTypes[type]);
        }
        xhr.send(null);
      });
    }
  }
})();

export default DataLoader;
