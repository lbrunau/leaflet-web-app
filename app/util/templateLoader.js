import Mustache from "mustache";
import DataLoader from "./dataLoader.js";

const TemplateLoader =(() => {
  const templateBasePath = "templates/";
  const cache = {

  };

  return {
    load: (templateName) => {
      if(cache[templateName]) {
        return cache[templateName];
      }
      return DataLoader.getRequest(templateBasePath + templateName, 'text/plain').then((data) => {
        cache[templateName] = data;
        return data;
      });
    },
    render: (templateName, data) => {
      if(cache[templateName]) {
        return Mustache.render(cache[templateName], data);
      }
      return '';
    }
  }
})();

export default TemplateLoader;
